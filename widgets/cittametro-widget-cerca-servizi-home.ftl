<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.currentPage param="title" var="titolo" />
<@wp.pageWithWidget widgetTypeCode="cagliari_widget_advsearch_results" var="searchWidgetPage" />

<div class="widget" data-ng-cloak data-ng-controller="ctrlRicerca as ctrl"> 
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></h3>
			</div>
		</div>
		<div class="row">
			<div class="offset-lg-2 col-lg-8 offset-lg-2 offset-md-1 col-md-10 offset-md-1 col-sm-12">
				<div class="box-servizi">
					<form action="<@wp.url page="${searchWidgetPage.code}"/>" method="post">
						<div class="form-group">
							<label class="sr-only" for="cerca-txt-intro"><@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_INFORMATIONPEOPLESERVICES" /></label>
							<input type="text" id="cerca-txt-intro" name="cercatxt" class="form-control" data-ng-model="ctrl.Ricerca.searchStringRicercaTxt" placeholder="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_INFORMATIONPEOPLESERVICES" />">
							<button type="submit" class="ico-sufix" >
								<svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
							</button>
						</div>
						<div>
							<a class="btn btn-default btn-verde" href="<@wp.url page="doc_01" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="doc_01" info="title" />"><@wp.pageInfo pageCode="doc_01" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="amm_03" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="amm_03" info="title" />"><@wp.pageInfo pageCode="amm_03" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="organizzazione" />?contentId=ORG9607" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: URP">URP </a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="persona" />?contentId=PRS11084" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: Sindaco">Sindaco</a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="amm_01_02" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="amm_01_02" info="title" />"><@wp.pageInfo pageCode="amm_01_02" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="doc_09" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="doc_09" info="title" />"><@wp.pageInfo pageCode="doc_09" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="doc_09_02" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="doc_09_02" info="title" />"><@wp.pageInfo pageCode="doc_09_02" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="ts_10_01" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="ts_10_01" info="title" />"><@wp.pageInfo pageCode="ts_10_01" info="title" /></a>
							<a class="btn btn-default btn-verde" data-ng-click="ctrl.Ricerca.setActive(); ctrl.General.setFiltriMode(true, true)" aria-label="<@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" />" data-toggle="modal" data-target="#searchModal">...</a>
						</div>
						<input type="hidden" name="amministrazione" value="{{ctrl.Ricerca.get_ammins_selected()}}">
						<input type="hidden" name="servizi" value="{{ctrl.Ricerca.get_servizi_selected()}}">
						<input type="hidden" name="novita" value="{{ctrl.Ricerca.get_novita_selected()}}">
						<input type="hidden" name="documenti" value="{{ctrl.Ricerca.get_docs_selected()}}">
						<input type="hidden" name="argomenti" value="{{ctrl.Ricerca.get_args_selected()}}">
						<input type="hidden" name="attivi" value="{{ctrl.Ricerca.active_chk}}">
						<input type="hidden" name="inizio" value="{{ctrl.Ricerca.data_inizio}}">
						<input type="hidden" name="fine" value="{{ctrl.Ricerca.data_fine}}">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>