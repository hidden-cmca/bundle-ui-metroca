<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpadmin=JspTaglibs["/apsadmin-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<li><a aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_CULTURE" />" href="<@wp.url page="arg_01_dett" />?categoryCode=317" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_CULTURE" />"><@wp.i18n key="CITTAMETRO_PORTAL_CULTURE" /></a></li>
<li><a aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_SPORT" />" href="<@wp.url page="arg_01_dett" />?categoryCode=4245" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_SPORT" />"><@wp.i18n key="CITTAMETRO_PORTAL_SPORT" /></a></li>
<li><a aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_TOURISM" />" href="<@wp.url page="arg_01_dett" />?categoryCode=4470" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_TOURISM" />"><@wp.i18n key="CITTAMETRO_PORTAL_TOURISM" /></a></li>
<li><a aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_CONTENT_ALLARGUMENTS" />" href="<@wp.url page="argomenti" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="argomenti" info="title" />"><@wp.i18n key="CITTAMETRO_CONTENT_ALLARGUMENTS" /></a></li>