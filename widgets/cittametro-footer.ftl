<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>

<#assign "livelloPrecedente" = -1 />
<#assign "npagine" = 0 />
<#assign "paginapadre" = ""/>

<@wp.nav var="pagina">

<#assign "livello" = pagina.level/>
<#if livello == 0>
  <#if -1 < livelloPrecedente >
    </ul>
    </div>
  </#if>

  <div class="col-lg-3 col-md-3 col-sm-6">
    <h4>
      <#if pagina.code == "amministrazione" > <#assign icona="account_balance"/> </#if>
      <#if pagina.code == "servizi" > <#assign icona="settings"/> </#if>
      <#if pagina.code == "novita" > <#assign icona="event"/> </#if>
      <#if pagina.code == "documenti" > <#assign icona="description"/> </#if>
      <!--
      <a href="${pagina.url}" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" />:  ${pagina.title}"> ${pagina.title} </a>
      -->
      <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-${icona}"></use></svg>${pagina.title}
      <#assign paginapadre = pagina.title/>
    </h4>
    <ul class="footer-list clearfix">
  </#if>
    <#if livello gt 0 >
    <li> 
      <a href="${pagina.url}" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${paginapadre}" > ${pagina.title}</a>
    </li>
      <#assign npagine = npagine + 1 />
    </#if>
      <#assign livelloPrecedente = livello />
    </@wp.nav>
    <#if npagine gt 0 >
    </ul>
  </div>
</#if>
