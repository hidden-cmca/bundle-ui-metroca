<#assign wp=JspTaglibs["/aps-core"]> <#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/metroca-core.tld"]>
<@wp.currentPage param="code" var="paginaCorrente" />
<@wp.freemarkerTemplateParameter var="paginaCorrente" valueName="paginaCorrente" />
<@uc.metrocaPageInfoTag pageCode="${paginaCorrente}" info="pathArray" var="pathCorrente"/> 

<@wp.nav var="pagina"> 
<#assign aperto="false"/>

<#list pathCorrente as pgitem > 
<#if pgitem == pagina.code> 
<#assign aperto="true"/>
 </#if>
</#list>

<#if aperto=="true">
<a class="btn btn-default btn-trasparente active" href="${pagina.url}" title="${pagina.title}"> ${pagina.title} </a> 

<#else>
<a class="btn btn-default btn-trasparente" href="${pagina.url}" title="${pagina.title}"> ${pagina.title} </a> 

</#if>

</@wp.nav>
</ul>
