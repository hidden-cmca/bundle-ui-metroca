<#assign wp=JspTaglibs["/aps-core"]>
<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>

<@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.carousel.min.css" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.theme.default.min.css" />

<#assign js_slide="$(document).ready(function() {        
	var owl = $('#owl-help');
	owl.owlCarousel({
		nav:false,
		startPosition: 0,
		autoPlay:false,
		responsiveClass:true,
		responsive:{
				0:{
					items:1,
				},
				576: {
					items:1,
				},
				768: {
					items:2,
				},
				991: {
					items:2,
				},
			}
	});	
});" />
<@wp.headInfo type="JS_CA_BTM_INC" info="${js_slide}" />


<section id="help">
	<div class="bg-help">
		<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/foto/ufficiaperti.jpg" alt="Assitenza uffici aperti" />
	</div>
	<div class="container">
		<div class="row">
			<div class="offset-lg-3 col-lg-6 col-md-12 text-center">
				<h3><@wp.i18n key="CITTAMETRO_WIDGET_HELP_TITLE" /></h3>
				<p><@wp.i18n key="CITTAMETRO_WIDGET_HELP_DESC" /></p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row row-eq-height">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<div id="owl-help" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
					<div class="item">
						<article class="scheda scheda-help scheda-round">
							<div class="scheda-testo">
								<h4>
									<span class="scheda-icona-grande"><svg class="icon1"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-cittadini"></use></svg></span>
									<a title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> <@wp.i18n key="CITTAMETRO_WIDGET_HELP_CITTADINI_TITLE" />"><@wp.i18n key="CITTAMETRO_WIDGET_HELP_CITTADINI" /></a>
								</h4>
								<p><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-phone_in_talk"></use></svg> <strong>800 016 058</strong></p>
								<p><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-whatsapp"></use></svg> <strong>+39 329 0582872</strong></p>
								<@jacms.content contentId="ORG9607" modelId="220022" />
							</div>
							<div class="scheda-footer">
								<span><@wp.i18n key="CITTAMETRO_WIDGET_HELP_COMPILA" /></span><br />
								<a href="<@wp.url page="scrivi_comune" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> <@wp.i18n key="CITTAMETRO_WIDGET_HELP_CITTADINI_TITLE" />" class="tutte"><@wp.i18n key="CITTAMETRO_WIDGET_HELP_CITTADINI_TITLE" /> <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
							</div>
						</article>
					</div>
					<div class="item">
						<article class="scheda scheda-help scheda-round">
							<div class="scheda-testo">
								<h4>
									<span class="scheda-icona-grande"><svg class="icon2"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-pa"></use></svg></span>
									<a title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> <@wp.i18n key="CITTAMETRO_WIDGET_HELP_PA_TITLE" />"><@wp.i18n key="CITTAMETRO_WIDGET_HELP_PA" /></a>
								</h4>
								<p><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-phone_in_talk"></use></svg> <strong>800 016 058</strong></p>
								<p><strong>&nbsp;</strong></p>
								<@jacms.content contentId="ORG9607" modelId="220022" />
							</div>
							<div class="scheda-footer">
								<span><@wp.i18n key="CITTAMETRO_WIDGET_HELP_COMPILA" /></span><br />
								<a title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> <@wp.i18n key="CITTAMETRO_WIDGET_HELP_PA_TITLE" />" class="tutte"><@wp.i18n key="CITTAMETRO_WIDGET_HELP_PA_TITLE" /> <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
							</div>
						</article>
					</div>
				</div>
			</div>		
		</div>
	</div>
</section>