<#assign wp=JspTaglibs["/aps-core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/metroca-core.tld"]>

<@wp.currentPage param="code" var="paginaCorrente" />
<@wp.freemarkerTemplateParameter var="paginaCorrente" valueName="paginaCorrente" />
<@uc.metrocaPageInfoTag pageCode="${paginaCorrente}" info="pathArray" var="pathCorrente"/> 
<@wp.nav var="pagina">
 <#assign aperto="false"/>
 <#list pathCorrente as pgitem > 
<#if pgitem == pagina.code> 
<#assign aperto="true"/>
 </#if>
 </#list>
<li <#if aperto=="true">class="active"</#if> > 
<a href="${pagina.url}" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" />:  ${pagina.title}"> ${pagina.title} </a> 
</li>
</@wp.nav>
