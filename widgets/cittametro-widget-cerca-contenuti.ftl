<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<@wp.currentPage param="title" var="titolo" />
<@wp.currentPage param="code" var="page" />
<@wp.pageWithWidget widgetTypeCode="cagliari_widget_advsearch_results" var="searchWidgetPage" />
<@cw.configWidget pageCode="${searchWidgetPage.code}" widgetCode="cagliari_widget_advsearch_results" configParam="contentTypesFilter" var="tipiContenuto" />

<form data-ng-cloak data-ng-controller="ctrlRicerca as ctrl" action="<@wp.url page="${searchWidgetPage.code}"/>" method="post">
	<div class="form-group" data-ng-init="ctrl.Ricerca.setContentTypes('${tipiContenuto}')">
		<label class="sr-only" for="cerca-intro"><@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /> ${titolo}</label>
		<input type="text" id="cerca-intro" name="cercatxt" class="form-control" data-ng-model="ctrl.Ricerca.searchStringRicercaTxt" placeholder="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /> ${titolo?lower_case}">
		<button type="submit" class="ico-sufix" value="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" />" title="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" />">
			<svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
		</button>
	</div>
	<div class="form-filtro">
		<div class="form-filtro-titoletto"><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></div>
		<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_tutti()" data-ng-click="ctrl.Ricerca.toggleAll()"><@wp.i18n key="CITTAMETRO_PORTAL_ALL" /></a>
		<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_ammins_selected(), ctrl.Ricerca.get_ammins_chk())" data-ng-click="ctrl.Ricerca.ammins_toggleAll()" data-ng-if="ctrl.Ricerca.ammins_isChecked()"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-account_balance"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" /></a>
		<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_servizi_selected(), ctrl.Ricerca.get_servizi_chk())" data-ng-click="ctrl.Ricerca.servizi_toggleAll()" data-ng-if="ctrl.Ricerca.servizi_isChecked()"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-settings"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></a>
		<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_novita_selected(), ctrl.Ricerca.get_novita_chk())" data-ng-click="ctrl.Ricerca.novita_toggleAll()" data-ng-if="ctrl.Ricerca.novita_isChecked()"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-event"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" /></a>
		<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_docs_selected(), ctrl.Ricerca.get_docs_chk())" data-ng-click="ctrl.Ricerca.docs_toggleAll()" data-ng-if="ctrl.Ricerca.docs_isChecked()"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-description"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" /></a>
		<div data-ng-if="!ctrl.Ricerca.ammins_isChecked()" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_ammins_selected())" data-ng-repeat="item in ctrl.Ricerca.get_ammins_chk()" class="btn-badge active">
			<span data-ng-bind-html="item.name">-</span>
			<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_ammins_selected())" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> {{ item.name }}"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="!ctrl.Ricerca.servizi_isChecked()" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_servizi_selected())" data-ng-repeat="item in ctrl.Ricerca.get_servizi_chk()" class="btn-badge active">
			<span data-ng-bind-html="item.name">-</span>
			<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_servizi_selected())" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> {{ item.name }}"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="!ctrl.Ricerca.novita_isChecked()" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_novita_selected())" data-ng-repeat="item in ctrl.Ricerca.get_novita_chk()" class="btn-badge active">
			<span data-ng-bind-html="item.name">-</span>
			<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_novita_selected())" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> {{ item.name }}"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="!ctrl.Ricerca.docs_isChecked()" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_docs_selected())" data-ng-repeat="item in ctrl.Ricerca.get_docs_chk()" class="btn-badge active">
			<span data-ng-bind-html="item.name">-</span>
			<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_docs_selected())" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> {{ item.name }}"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>		
		<div data-ng-if="ctrl.Ricerca.get_args_selected().length !== ctrl.Ricerca.get_argomenti().length" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()" 
			class="btn-badge active">
			<span data-ng-bind-html="item.name">-</span>
			<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_args_selected())" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" />  {{ item.name }}">
				<svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg>
			</a>
		</div>
		<div data-ng-if="ctrl.Ricerca.activeChk" class="btn-badge active">
			<@wp.i18n key="CITTAMETRO_CONTENT_ACTIVE_CONTENT" /> <a href="" data-ng-click="ctrl.Ricerca.activeChk = false" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="btn-badge active">
			<@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }}&nbsp;<@wp.i18n key="CITTAMETRO_PORTAL_TO" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> <@wp.i18n key="CITTAMETRO_PORTAL_FROM" /> <@wp.i18n key="CITTAMETRO_PORTAL_TO" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="ctrl.Ricerca.dataInizio && !ctrl.Ricerca.dataFine" class="btn-badge active">
			<@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }} <a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> <@wp.i18n key="CITTAMETRO_PORTAL_START_DATE" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="!ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="btn-badge active">
			<@wp.i18n key="CITTAMETRO_PORTAL_UNTIL" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" />" <@wp.i18n key="CITTAMETRO_PORTAL_END_DATE" />><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<a href="" class="btn btn-default btn-trasparente" data-ng-click="ctrl.Ricerca.setActive(); ctrl.General.setFiltriMode(true, true, '${page}')" aria-label="<@wp.i18n key="SEARCH_FILTERS_BUTTON" />" data-toggle="modal" data-target="#searchModal">...</a>
	</div>
	<input type="hidden" name="amministrazione" value="{{ctrl.Ricerca.get_ammins_selected()}}">
	<input type="hidden" name="servizi" value="{{ctrl.Ricerca.get_servizi_selected()}}">
	<input type="hidden" name="novita" value="{{ctrl.Ricerca.get_novita_selected()}}">
	<input type="hidden" name="documenti" value="{{ctrl.Ricerca.get_docs_selected()}}">
	<input type="hidden" name="argomenti" value="{{ctrl.Ricerca.get_args_selected()}}">
	<input type="hidden" name="attivi" value="{{ctrl.Ricerca.activeChk}}">
	<input type="hidden" name="inizio" value="{{ctrl.Ricerca.dataInizio}}">
	<input type="hidden" name="fine" value="{{ctrl.Ricerca.dataFine}}">
</form>
