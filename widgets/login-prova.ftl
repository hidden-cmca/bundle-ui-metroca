<#assign wp=JspTaglibs["/aps-core"]>

  <#if (Session.currentUser != "guest")>
   <div class="btn-group">   
   <button type="button" class="btn btn-default btn-accedi btn-accedi-ap-md" role="button" id="dropdownAP" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false"> ${Session.currentUser} <span class="sr-only">${Session.currentUser}</span>    
  </button>    

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
 <div class="link-list-wrapper">
<ul class="link-list">
  <@wp.ifauthorized permission="enterBackend">        
<li>
     <a class="list-item" href="/app-builder/">  <@wp.i18n key="Area Personale" />        </a> 
</li>
<li>
      <a class="list-item" href="/app-builder/user/edit/${Session.currentUser}" >  <@wp.i18n key="Gestione Utente" />    </a>      
    </@wp.ifauthorized>       
</li>
<li>
 <a class="list-item"  href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action">  <@wp.i18n key="ESLF_SIGNOUT" />    </a>
<@wp.pageWithWidget var="editProfilePageVar" widgetTypeCode="userprofile_editCurrentUser" />
<#if (editProfilePageVar??) >
<a class="list-item" href="<@wp.url page="${editProfilePageVar.code}" />" ><@wp.i18n key="ESLF_PROFILE_CONFIGURATION" /></a>
</#if>
</li>
</ul>
</div>
</div>
</div>

<#else>
<script nonce="<@wp.cspNonce />">
  function login() {
   window.entando.keycloak.login();
   location.href = '<@wp.info key="systemParam" paramName="applicationBaseURL" />do/login?redirectTo=<@wp.url page="homepage"/>';
  }
  document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('login-button')
      .addEventListener('click', login);
  });
  </script>
  <script nonce="<@wp.cspNonce />">window.addEventListener("user.form.login", (evt) => {  if(evt.detail.payload){    login();  }});</script>
  <div class="btn-group login-drop">    
  <button id="login-button" type="button" class="btn btn-default btn-accedi btn-accedi-ap-md" >   
 <@wp.i18n key="ESLF_SIGNIN" />   
 </button>    
<!--
<a class="btn sign-up " style="line-height: 37px;" href="<@wp.url page="sign_up" />" >       
<@wp.i18n key="SIGN_UP" /> -->   
 </a>
 </div> 
</#if>
