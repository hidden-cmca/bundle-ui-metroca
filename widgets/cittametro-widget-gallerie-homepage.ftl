<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.carousel.min.css" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.theme.default.min.css" />

<#assign js_slide="$(document).ready(function() {        
	var owl = $('#owl-galleria-home');
	owl.owlCarousel({
		nav:false,
		startPosition: 0,
		autoPlay:false,
		responsiveClass:true,
		responsive:{
				0:{
					items:1,
				},
				576: {
					items:1,
				},
				768: {
					items:2,
				},
				991: {
					items:3,
				},
			}
	});
});" />
<@wp.headInfo type="JS_CA_BTM_INC" info="${js_slide}" />

<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />
<section id="gallerie">
<div class="container">
	<div class="widget">
		<#if (titleVar??)>
			<div class="row">
				<div class="col-md-12">
					<div class="titolosezione">
						<h3>${titleVar}</h3>
					</div>
				</div>
			</div>
		</#if>
		<#if (contentInfoList??) && (contentInfoList?has_content) && (contentInfoList?size > 0)>
			<@wp.pager listName="contentInfoList" objectName="groupContent" pagerIdFromFrame=true advanced=true offset=5>
				<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
					<div id="owl-galleria-home" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
						<#list contentInfoList as contentInfoVar>
							<#assign cid = contentInfoVar['contentId']>
							<#if (cid?contains("GAL"))>
								<@jacms.content contentId="${contentInfoVar['contentId']}" modelId="300002" />
							</#if>
							<#if (cid?contains("VID"))>
								<@jacms.content contentId="${contentInfoVar['contentId']}" modelId="190005" />
							</#if>
						</#list>
					</div>
				</@wp.freemarkerTemplateParameter>
			</@wp.pager>
		</#if>
		<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
			<div class="row">
				<div class="col-md-12 mt16">
					<div class="text-center">
						<#if (pageLinkVar??)>
							<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="btn btn-default btn-verde mr0">${pageLinkDescriptionVar}</a>
						<#else>
							${titleVar}
						</#if>
					</div>
				</div>
			</div>
		</#if>
		<#assign contentInfoList="">
	</div>
</div>
</section>