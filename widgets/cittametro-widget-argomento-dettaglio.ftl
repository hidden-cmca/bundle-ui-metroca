<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<@cw.currentArgument var="currentArg" />
<#if (currentArg??)>
    <@cw.contentList listName="contentList" contentType="ARG" category="${currentArg}"/>
    <#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>
        <#list contentList as contentId>
            <@jacms.content contentId="${contentId}" />
        </#list>
    <#else>
    	<p class="alert alert-info"><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
    </#if>
</#if>
<#assign contentList="">