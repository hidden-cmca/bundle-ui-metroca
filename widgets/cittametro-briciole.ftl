<#assign wp=JspTaglibs["/aps-core"]> 
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/metroca-core.tld"]>

<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="breadcrumb">
					<ol class="breadcrumb">
                                                <@wp.currentPage param="code" var="currentViewCode" />
                                                <@wp.freemarkerTemplateParameter var="currentViewCode" valueName="currentViewCode" />
						<#assign first=true />
						<#assign lastPageTitle="" />
						
						<@uc.metrocaMainFrameContentTag var="content"/>
						<@uc.metrocaPageInfoTag pageCode="${currentViewCode}" info="isPublishOnFly" var="isPublishOnFly"/>
						<@wp.nav spec="current.path" var="currentTarget">
							<#assign currentCode = currentTarget.code />
							<#if !currentTarget.voidPage>
								<#if currentCode == currentViewCode>
									<#if (isPublishOnFly == "true")>
										<#assign title=content.getAttributeByRole("jacms:title").text>
										<li class="breadcrumb-item active" aria-current="page"><a>${title}</a></li>
									<#else>									
										<li class="breadcrumb-item active" aria-current="page"><a>${currentTarget.title}</a></li>
									</#if>
								<#else>
									<li class="breadcrumb-item"><a href="${currentTarget.url}" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" /> ${currentTarget.title}">${currentTarget.title}</a><span class="separator">/</span></li>
									<#if (!first)>
										
									</#if>
								</#if>
							<#else>
								<li class="breadcrumb-item active" aria-current="page"><a>${currentTarget.title}</a></li>
							</#if>
							<#assign lastPageTitle = currentTarget.title />
						</@wp.nav>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
