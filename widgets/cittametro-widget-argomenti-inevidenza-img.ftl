<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<#assign utenteToken = '' />
<#if (Session.currentUser?? && Session.currentUser != "guest")>
<#if (Session.currentUser.accessToken??)>
<#assign utenteToken = Session.currentUser.accessToken />
</#if>
</#if>

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />
<@wp.categories var="systemCategories" titleStyle="prettyFull" />

<#assign tipoContenuto = 'ARG' />
<#assign idModello = '400006' />
<#assign elementiMax = 999 />
<#assign elementiMaxItem = 7 />
<#assign ordinamento = "(order=ASC;attributeFilter=true;key=titolo)" />
<#assign categorie = "evd" />

<div class="widget" data-ng-controller="FiltriController">
	<div data-ng-cloak data-ng-init="setParameters('${elementiMaxItem}', '${elementiMax}', '${tipoContenuto}', '${idModello}', '${categorie}', '${tipoContenuto}', '', '${ordinamento}', '', '${utenteToken}')"> 
		<div class="row">
			<div class="col-md-12">
				<div class="titolosezione">
					<h3><@wp.i18n key="CITTAMETRO_PORTAL_INEVIDENCE" /></h3>
				</div>
			</div>
		</div>
		
		<div data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
			<div class="row row-eq-height" data-ng-if="($index+1) == '${elementiMaxItem}' || (($index+1) + ('${elementiMaxItem}'* currentPage)) == contents.length">
				<div class="col-md-4" data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) >= 1">
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) == 1" data-ng-init="getContent(elem.$, '${idModello}')" data-ng-bind-html="renderContent[elem.$]['${idModello}']"></div>
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) == 2">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, '${idModello}')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$]['${idModello}']"></div>
						<div data-ng-init="getContent(elem.$, '400008')" data-ng-bind-html="renderContent[elem.$]['400008']"></div>
					</div>
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) == 3">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, '${idModello}')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$]['${idModello}']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, '400008')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$]['400008']"></div>
					</div>
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) == 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, '${idModello}')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$]['${idModello}']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, '400008')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$]['400008']"></div>
					</div>
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) == 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, '${idModello}')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$]['${idModello}']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, '400008')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$]['400008']"></div>
					</div>
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) == 6">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-5].$, '${idModello}')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-5].$]['${idModello}']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, '400008')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$]['400008']"></div>
					</div>
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) >= 7">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-6].$, '${idModello}')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-6].$]['${idModello}']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-5].$, '400008')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-5].$]['400008']"></div>
					</div>
				</div>
				
				<div class="col-md-4" data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) >= 2">
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) == 3" data-ng-init="getContent(elem.$, '400007')" data-ng-bind-html="renderContent[elem.$]['400007']"></div>
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) == 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, '400007')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$]['400007']"></div>
						<div data-ng-init="getContent(elem.$, '400009')" data-ng-bind-html="renderContent[elem.$]['400009']"></div>
					</div>
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) == 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, '400007')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$]['400007']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, '400009')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$]['400009']"></div>
						<div data-ng-init="getContent(elem.$, '400007')" data-ng-bind-html="renderContent[elem.$]['400007']"></div>
					</div>
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) == 6">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, '400007')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$]['400007']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, '400009')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$]['400009']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, '400007')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$]['400007']"></div>
					</div>
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) >= 7">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, '400007')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$]['400007']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, '400009')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$]['400009']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, '400007')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$]['400007']"></div>
					</div>
				</div>
				
				<div class="col-md-4" data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) >= 6">
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) == 6" data-ng-init="getContent(elem.$, '400008')" data-ng-bind-html="renderContent[elem.$]['400008']"></div>
					<div data-ng-if="(contents.length - ('${elementiMaxItem}'* currentPage)) >= 7">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, '400008')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$]['400008']"></div>
						<div data-ng-init="getContent(elem.$, '${idModello}')" data-ng-bind-html="renderContent[elem.$]['${idModello}']"></div>
					</div>
				</div>
				
			</div>
		</div>
		
		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && ${elementiMax}>${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" /> - <@wp.i18n key="CITTAMETRO_PORTAL_INEVIDENCE" />">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="goToPage(currentPage-1)" 
								title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+5 && elem-1>currentPage-5" title="{{elem}}"
							data-ng-class="currentPage == (elem-1) ? 'page-item active' : 'page-item'">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="goToPage(elem-1)">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="goToPage(elem-1)">{{elem}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage >= contents.length/pageSize - 1" data-ng-click="goToPage(currentPage+1)"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		
		<div class="row row-eq-height" data-ng-if="contents.length == 0">
			<div class="col-md-12">
				<p><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
			</div>
		</div>
	</div>
	<#assign contentList="">
</div>