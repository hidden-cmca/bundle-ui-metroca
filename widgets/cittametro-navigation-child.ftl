<#assign wp=JspTaglibs["/aps-core"]> 
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/metroca-core.tld"]>

<@wp.currentPage param="code" var="paginaCorrente" />

<#assign "livelloCorrente"=-1/>
<@wp.nav spec="code(homepage).subtree(20)" var="cercaPagina" >
    <#if cercaPagina.code == paginaCorrente >
        <#assign livelloCorrente=cercaPagina.level />
    </#if>
</@wp.nav>
<@wp.nav spec="code(amministrazione_trasparente).subtree(10)" var="cercaPagina2" >
    <#if cercaPagina2.code == paginaCorrente>
        <#assign livelloCorrente = cercaPagina2.level + 1 />
    </#if>
</@wp.nav>

<@wp.pageInfo pageCode=paginaCorrente info="hasChild" var="hafigli" />

<#if hafigli=="true">

    <#assign "numerononvolanti"=0 />
    <@wp.nav spec="code(${paginaCorrente}).subtree(1)" var="cercaFigliNonVolanti" >
        <@uc.metrocaPageInfoTag pageCode=cercaFigliNonVolanti.code info="isPublishOnFly" var="isPublishOnFly"/>
       
        <#if  (isPublishOnFly=="false") && (cercaFigliNonVolanti.code != paginaCorrente ) >
            <#assign numerononvolanti=numerononvolanti+1 />
        </#if>
    </@wp.nav>

    <#if  numerononvolanti == 0>
         
         <#assign hafigli="false" />
    </#if>
</#if>
<#if  hafigli="false">
    <@uc.metrocaPageInfoTag pageCode=paginaCorrente pageVar="paginaPadre" var="pg" />
    <#assign paginaCorrente=paginaPadre.parentCode />
    <#assign livelloCorrente=livelloCorrente-1/>
</#if>

<nav>
    <h4>
        <#assign "etichetta_titolo"="CITTAMETRO_PORTAL_ALL" />
        <#if livelloCorrente == 1>
          <#assign "etichetta_titolo"= etichetta_titolo + paginaCorrente ?upper_case/>
            <@wp.i18n key=etichetta_titolo  />
        </#if>
        <#if livelloCorrente gt 1 || livelloCorrente == -1 >
            <@wp.pageInfo pageCode=paginaCorrente info="title" var="titoloPaginaCorrente" />
            ${titoloPaginaCorrente}
        </#if>
    </h4>
    <#assign "npagine"=0 />
    <ul class="list-group">
        <@wp.nav var="pagina" spec="code(${paginaCorrente}).subtree(1)">
            <#assign "link"=pagina.url />
            <#if pagina.code != paginaCorrente >
                <#if npagine == 5>
                    </ul><ul class="list-group collapse" id="menu-lista-altre">
                </#if>
                <li class="list-group-item">     
                    <a href="${link}" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> ${pagina.title} ">
                        ${pagina.title}
                    </a>
                </li>
                <#assign "npagine" = npagine+1 />
            </#if>
        </@wp.nav>
    </ul>
    <#if npagine gt 5>
        <p class="list-group-item">     
            <a href="#menu-lista-altre" id="menu-lista-button" role="button" aria-expanded="false" aria-controls="menu-lista-altre" data-toggle="collapse">
                <svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-more_vert"></use></svg>
                <span class="menu-lista-apri" title="<@wp.i18n key="CITTAMETRO_PORTAL_EXPAND" />"><@wp.i18n key="CITTAMETRO_PORTAL_EXPAND" /></span>
                <span class="menu-lista-chiudi" title="<@wp.i18n key="CITTAMETRO_PORTAL_CLOSE" />"><@wp.i18n key="CITTAMETRO_PORTAL_CLOSE" /></span>
            </a>
        </p>
    
        <p class="list-group-item mt16">
            <#assign "frameattivo"="sezioni-servizi" />
            <@wp.currentWidget frame=6 var="widget" param="code" />
            <#if widget??>
                <#assign "frameattivo"="sezioni-inevidenza"/>
            </#if>
            <a href="<@wp.url page=paginaCorrente />#${frameattivo}" title="<@wp.i18n key="ENTANDO_API_GOTO_DETAILS" />">
                <strong><@wp.i18n key="CITTAMETRO_PORTAL_SEEALL" /></strong>
            </a>
        </p>
    </#if>
</nav>
