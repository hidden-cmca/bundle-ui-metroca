<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>  
 
<section class="footer-logo">
 	<div class="row clearfix">
 		<div class="col-sm-12 intestazione">
 			<div class="logoimg">
 				<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />">
 					<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/logo_cittametro_cagliari.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT	" />"/>
 				</a>
 			</div>
 			<div class="logotxt">
 				<h3>
 					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" /> <@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" /></a>
 				</h3>
 			</div>
 		</div>
 	</div>
</section>