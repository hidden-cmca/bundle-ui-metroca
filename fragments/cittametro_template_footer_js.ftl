<#assign wp=JspTaglibs[ "/aps-core"]>
<#-- FOOTER SCRIPTS -->
<script nonce="<@wp.cspNonce />">window.__PUBLIC_PATH__ ="<@wp.resourceURL />cittametro-homepage-bundle/static/font/" </script>
<#-- <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/bootstrap-italia.bundle_1.4.3.min.js" ></script> -->
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/cittametro.js" ></script>




<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/popper.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/bootstrap-italia_1.2.0sf.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery-ui.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/i18n/datepicker-it.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-animate.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-aria.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-messages.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-sanitize.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/app.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/ricerca.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/ricerca-service.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/general-service.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-material.cagliari.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery.cookie.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery.cookiecuttr.min.js"></script>



<@wp.outputHeadInfo type="JS_CA_BTM_EXT"><script nonce="<@wp.cspNonce />" src="<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="JS_CA_BTM_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="JS_CA_BTM"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="JS_CA_BTM_PLGN"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>

<@wp.outputHeadInfo type="JS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="JS_CA_TOP"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/leafletjs_config.min.js"></script></@wp.outputHeadInfo>


<script nonce="<@wp.cspNonce />">
<@wp.outputHeadInfo type="JS_CA_BTM_INC">
<@wp.printHeadInfo />
</@wp.outputHeadInfo>
</script>



<!-- COOKIE BAR, ANALITICS e HOTJAR -->
<script nonce="<@wp.cspNonce />">
(function ($) {
$(document).ready(function () {
// activate cookie cutter
$.cookieCuttr({
cookieAnalytics: false,
cookieMessage: '<p><@wp.i18n key="CITTAMETRO_FOOTER_COOKIES_TEXT" />&nbsp;<a href="<@wp.url page="privacy" />" class="text-white" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> <@wp.i18n key="CITTAMETRO_PORTAL_PRIVACYPOLICY" />"><@wp.i18n key="CITTAMETRO_PORTAL_PRIVACYPOLICY" /></a>.<br/><@wp.i18n key="CITTAMETRO_FOOTER_COOKIES_QUESTION" /></p>',
cookieAcceptButtonText: '<@wp.i18n key="CITTAMETRO_FOOTER_ACCEPTS_COOKIES" />',
cookieDiscreetPosition: "bottomleft",
cookieDeclineButton: true,
//cookieResetButton: true,
cookieDeclineButtonText: '<@wp.i18n key="CITTAMETRO_FOOTER_DECLINES_COOKIES" />',
//cookieResetButtonText: 'Cambia scelta cookies',
cookieDomain: 'comune.cagliari.it'
});
});
})(jQuery);

if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept") {
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-27957186-1', 'auto');
ga('set', 'anonymizeIp', true);
ga('send', 'pageview');



(function(h,o,t,j,a,r){
h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
h._hjSettings={hjid:553641,hjsv:6};
a=o.getElementsByTagName('head')[0];
r=o.createElement('script');r.async=1;
r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
a.appendChild(r);
})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
}
</script>
<@wp.fragment code="cittametro_template_search_modal" escapeXml=false />