<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<@wp.categories var="systemCategories" root="arg_argomenti" />
<@wp.currentPage param="code" var="code" />
<@wp.pageWithWidget widgetTypeCode="cagliari_widget_advsearch_results" var="searchWidgetPage" />
<@cw.configWidget pageCode="${searchWidgetPage.code}" widgetCode="cagliari_widget_advsearch_results" configParam="contentTypesFilter" var="tipiContenuto" />
<#if (!tipiContenuto??)><#assign tipiContenuto = ""/></#if>

<div id="dvRicerca">
	<div data-ng-cloak data-ng-controller="ctrlRicerca as ctrl">
		<!-- CERCA -->
		<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalTitle" aria-hidden="false">
			<script nonce="<@wp.cspNonce />">
				$(function() {
				  $("#datepicker_start").datepicker();
				  $("#datepicker_end").datepicker();
				});
			</script>
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="ricerca" action="<@wp.url page="${searchWidgetPage.code}"/>" method="post">
						<div class="modal-header-fullsrc">
							<div class="container">
								<div class="row">
									<div class="col-sm-1" data-ng-class="{pb12: !ctrl.General.getFiltriMode().filtri}">
										<button data-ng-hide="ctrl.General.getFiltriMode().filtri && !ctrl.General.getFiltriMode().internal" type="button" class="close" data-dismiss="modal" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_CLOSE_SEARCH_FILTERS" />" data-ng-click="ctrl.General.setFiltriMode(false, false)">
											<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_back"></use></svg>
										</button>
										<button data-ng-show="ctrl.General.getFiltriMode().filtri && !ctrl.General.getFiltriMode().internal" type="button" class="close" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_CLOSE_SEARCH_FILTERS" />" data-ng-click="ctrl.General.setFiltriMode(false, false)">
											<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_back"></use></svg>
										</button>
									</div>
									<div class="col-sm-11">
										<h1 class="modal-title" id="searchModalTitle">
											<span data-ng-hide="ctrl.General.getFiltriMode().filtri"><@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /></span>
											<span data-ng-show="ctrl.General.getFiltriMode().filtri"><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></span>
										</h1>
										<button data-ng-show="ctrl.General.getFiltriMode().filtri" class="confirm btn btn-default btn-trasparente float-right"><@wp.i18n key="CITTAMETRO_PORTAL_CONFIRM" /></button>
									</div>
								</div>
							</div>
							<div class="search-filter-type ng-hide" data-ng-show="ctrl.General.getFiltriMode().filtri">
								<ul class="nav nav-tabs" role="tablist">
									<li role="presentation">
										<a data-ng-if="ctrl.General.getFiltriMode().categoria == ''" href="" aria-controls="categoriaTab" role="tab" data-toggle="tab" data-ng-click="ctrl.Ricerca.setActive('categorie')" data-ng-class="ctrl.Ricerca.get_categoriaTab()"><@wp.i18n key="CITTAMETRO_PORTAL_CATEGORIES" /></a>
										<a data-ng-if="ctrl.General.getFiltriMode().categoria != ''" href="" aria-controls="categoriaTab" role="tab" data-toggle="tab" data-ng-click="ctrl.Ricerca.setActive('categorie')" data-ng-class="ctrl.Ricerca.get_categoriaTab()">{{ctrl.General.getFiltriMode().categoria}}</a>
									</li>
									<li role="presentation"><a href="" aria-controls="argomentoTab" role="tab" data-toggle="tab"
										data-ng-click="ctrl.Ricerca.setActive('argomenti')" data-ng-class="ctrl.Ricerca.get_argomentoTab()"><@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" /></a></li>
									<li role="presentation"><a href="" aria-controls="opzioniTab" role="tab" data-toggle="tab"
										data-ng-click="ctrl.Ricerca.setActive('opzioni')" data-ng-class="ctrl.Ricerca.get_opzioniTab()"><@wp.i18n key="CITTAMETRO_CONTENT_OPTIONS" /></a></li>
								</ul>
							</div>
						</div>
						<div class="modal-body-search">
							<div class="container">
								<div class="row" data-ng-hide="ctrl.General.getFiltriMode().filtri">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="form-group" data-ng-init="ctrl.Ricerca.setContentTypes('${tipiContenuto}')">
											<label for="cerca-txt" class="sr-only active"><@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /></label>
											<md-autocomplete type="text" md-input-name ="cercatxt" md-input-id ="cerca-txt" placeholder="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_INFORMATIONPEOPLESERVICES" />" md-selected-item="ctrl.Ricerca.cercatxt"
												md-search-text="ctrl.Ricerca.searchStringRicercaTxt" md-selected-item-change="ctrl.Ricerca.selectedRicercaTxtItemChanged(item)" md-items="item in ctrl.Ricerca.getAutocompleteResults(ctrl.Ricerca.searchStringRicercaTxt)" md-item-text="item.contentName"	md-min-length="1" md-clear-button="true">
												<md-item-template>
													<div>
														<a data-ng-href="{{item.href}}" data-ng-if="!item.searchButton && item.searchButton != 'hidden'">
															<div>
																<svg class="icon icon-sm" data-ng-bind-html="item.icon"></svg>
																<span class="autocomplete-list-text">
																	<span data-md-highlight-text="ctrl.Ricerca.searchStringRicercaTxt" data-md-highlight-flags="i">{{item.contentName}}</span>
																	<em>{{item.category}}</em>
																</span>
															</div>
														</a>
														<div class="search-start" data-ng-if="item.searchButton && item.searchButton != 'hidden'" data-ng-click="item.contentName = ctrl.Ricerca.searchStringRicercaTxt" onclick="document.getElementById('search-button').click();">
															<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
															<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_THROUGHOUT_SITE" />
														</div>
														<div class="search-start-hidden" data-ng-if="item.searchButton == 'hidden'"> </div>
													</div>
												</md-item-template>
											</md-autocomplete>
											<svg class="icon ico-prefix"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
										</div>
										<div data-ng-hide="ctrl.Ricerca.getAutocompleteIsOpened()">
											<div class="form-filtro">
												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_tutti('categorie')" data-ng-click="ctrl.Ricerca.toggleAll('categorie')"><@wp.i18n key="CITTAMETRO_PORTAL_ALL" /></a>
												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_ammins_selected(), ctrl.Ricerca.get_ammins_chk())" data-ng-click="ctrl.Ricerca.ammins_toggleAll()"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-account_balance"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" /></a>
												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_servizi_selected(), ctrl.Ricerca.get_servizi_chk())" data-ng-click="ctrl.Ricerca.servizi_toggleAll()"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-settings"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></a>
												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_novita_selected(), ctrl.Ricerca.get_novita_chk())" data-ng-click="ctrl.Ricerca.novita_toggleAll()"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-event"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" /></a>
												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_docs_selected(), ctrl.Ricerca.get_docs_chk())" data-ng-click="ctrl.Ricerca.docs_toggleAll()"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-description"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" /></a>
												<a href="" class="btn btn-default btn-trasparente" data-ng-click="ctrl.Ricerca.setActive(); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_FILTER_BY" />&nbsp;<@wp.i18n key="CITTAMETRO_PORTAL_CATEGORIES" />">...</a>
											</div>
											<div class="form-blockopz">
												<h2><@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" /></h2>
												<a href="" data-ng-class="(ctrl.Ricerca.get_args_selected().length > 0 && ctrl.Ricerca.get_args_selected().length !== ctrl.Ricerca.get_argomenti().length)? 'badge badge-pill badge-argomenti' : 'badge badge-pill badge-argomenti active'" data-ng-click="ctrl.Ricerca.toggleAll('argomenti')">
													<@wp.i18n key="CITTAMETRO_CONTENT_ALLARGUMENTS" />
												</a>
												<div data-ng-if="ctrl.Ricerca.get_args_selected().length !== ctrl.Ricerca.get_argomenti().length" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()" class="badge badge-pill badge-argomenti active">
													<span data-ng-bind-html="item.name">-</span>
													<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_args_selected())" class="btn-badge-close">
														<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg>
													</a>
												</div>
												<a href="" class="badge badge-pill badge-argomenti puntini" data-ng-click="ctrl.Ricerca.setActive('argomenti'); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_FILTER_BY" />&nbsp;<@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" />">...</a>
											</div>
											<div class="form-blockopz">
												<h2><@wp.i18n key="CITTAMETRO_CONTENT_OPTIONS" /></h2>
												<a href="" data-ng-class="(ctrl.Ricerca.activeChk || ctrl.Ricerca.dataInizio || ctrl.Ricerca.dataFine)? 'badge badge-pill badge-argomenti' : 'badge badge-pill badge-argomenti active'" data-ng-click="ctrl.Ricerca.activeChk = false; ctrl.Ricerca.clean_date()">
													<@wp.i18n key="CITTAMETRO_CONTENT_ALLOPTIONS" />
												</a>
												<div data-ng-if="ctrl.Ricerca.activeChk" class="badge badge-pill badge-argomenti active">
													<@wp.i18n key="CITTAMETRO_CONTENT_ACTIVE_CONTENT" />&nbsp;<a href="" data-ng-click="ctrl.Ricerca.activeChk = false" class="btn-badge-close"><svg class="icon">
													<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
												</div>
												<div data-ng-if="ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">
													<@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }}&nbsp;<@wp.i18n key="CITTAMETRO_PORTAL_TO" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
												</div>
												<div data-ng-if="ctrl.Ricerca.dataInizio && !ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">
													<@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }}&nbsp;<a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
												</div>
												<div data-ng-if="!ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">
													<@wp.i18n key="CITTAMETRO_PORTAL_UNTIL" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
												</div>
												<a href="" class="badge badge-pill badge-argomenti puntini" data-ng-click="ctrl.Ricerca.setActive('opzioni'); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="SEARCH_FILTERS_BUTTON" />">...</a>
											</div>
											<button id="search-button" class="search-start" >
												<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
												<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_THROUGHOUT_SITE" />
											</button>
										</div>
										<input type="hidden" id="amministrazione" name="amministrazione" value="{{ctrl.Ricerca.get_ammins_selected()}}">
										<input type="hidden" id="servizi" name="servizi" value="{{ctrl.Ricerca.get_servizi_selected()}}">
										<input type="hidden" id="novita" name="novita" value="{{ctrl.Ricerca.get_novita_selected()}}">
										<input type="hidden" id="documenti" name="documenti" value="{{ctrl.Ricerca.get_docs_selected()}}">
										<input type="hidden" id="argomenti" name="argomenti" value="{{ctrl.Ricerca.get_args_selected()}}">
										<input type="hidden" id="attivi" name="attivi" value="{{ctrl.Ricerca.activeChk}}">
										<input type="hidden" id="inizio" name="inizio" value="{{ctrl.Ricerca.dataInizio}}">
										<input type="hidden" id="fine" name="fine" value="{{ctrl.Ricerca.dataFine}}">
									</div>
								</div>
								
								<#list systemCategories?sort_by("value") as systemCategory>
									<data-ng-container data-ng-init="ctrl.Ricerca.setArgomenti('${systemCategory.value?replace("'", "\\'")}', '${systemCategory.key}')"></data-ng-container>
								</#list>
								<@wp.nav var="pagina" spec="code(homepage).subtree(4)">
									<#if (pagina.level == 1)>
										<#assign padre = pagina.code />
									</#if>
									<#if (pagina.level == 2)>	
										<#assign padre2 = pagina.code />
										<#assign categoria = pagina.title />
										<#assign categoriaCode = pagina.code />
										<data-ng-container data-ng-init="ctrl.Ricerca.setCategorie('${categoria?replace("'", "\\'")}', '${categoriaCode}', '${padre}')"></data-ng-container>
									</#if>
									<#if (pagina.level > 2)>
										<#assign categoria = pagina.title />
										<#assign categoriaCode = pagina.code />
										<data-ng-container data-ng-init="ctrl.Ricerca.setCategorie('${categoria?replace("'", "\\'")}', '${categoriaCode}', '${padre}', '${padre2}')"></data-ng-container>
									</#if>
								</@wp.nav>
								
								<#if (code == 'amministrazione')><data-ng-container data-ng-init="ctrl.Ricerca.ammins_toggleAll()"></data-ng-container></#if>
								<#if (code == 'servizi')><data-ng-container data-ng-init="ctrl.Ricerca.servizi_toggleAll()"></data-ng-container></#if>
								<#if (code == 'novita')><data-ng-container data-ng-init="ctrl.Ricerca.novita_toggleAll()"></data-ng-container></#if>
								<#if (code == 'documenti')><data-ng-container data-ng-init="ctrl.Ricerca.docs_toggleAll()"></data-ng-container></#if>
								
								<div role="tabpanel" data-ng-show="ctrl.General.getFiltriMode().filtri" class="ng-hide" data-ng-init="showallcat = false; showallarg = false">
									<div class="tab-content">
										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_categoriaTab() == 'active'? 'tab-pane active' : 'tab-pane'" id="categoriaTab" data-ng-init="ctrl.Ricerca.chkCategoryCode('${code}')">
											<div class="row">
												<div class="offset-md-1 col-md-10 col-sm-12">
													<div class="search-filter-ckgroup">
														<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" />"
															data-ng-if="ctrl.General.getFiltriMode().categoria == ''"
															data-ng-checked="ctrl.Ricerca.ammins_isChecked()"
															md-indeterminate="ctrl.Ricerca.ammins_isIndeterminate()"
															data-ng-click="ctrl.Ricerca.ammins_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" /></label>
														</md-checkbox>
														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_ammins_chk()"
															data-ng-if="ctrl.General.getFiltriMode().categoria == 'Amministrazione' || ctrl.General.getFiltriMode().categoria == ''">
															<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_ammins_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_ammins_selected())" 	aria-label="{{item.name}}">
																<label data-ng-bind-html="item.name"></label>
															</md-checkbox>
														</div>
													</div>
													<div class="search-filter-ckgroup">
														<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" />"
															data-ng-if="ctrl.General.getFiltriMode().categoria == ''"
															data-ng-checked="ctrl.Ricerca.servizi_isChecked()"
															md-indeterminate="ctrl.Ricerca.servizi_isIndeterminate()"
															data-ng-click="ctrl.Ricerca.servizi_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></label>
														</md-checkbox>
														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_servizi_chk()"
															data-ng-if="ctrl.General.getFiltriMode().categoria == 'Servizi' || ctrl.General.getFiltriMode().categoria == ''">
															<div data-ng-if = "$index <= 2">
																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_servizi_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_servizi_selected())" aria-label="{{item.name}}">
																<label data-ng-bind-html="item.name"></label>
																</md-checkbox>
															</div>
															<div data-ng-if = "$index >= 3">									
																<div data-ng-show="showallcat || ctrl.General.getFiltriMode().categoria != ''">
																	<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_servizi_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_servizi_selected())" aria-label="{{item.name}}">
																	<label data-ng-bind-html="item.name"></label>
																	</md-checkbox>
																</div>
															</div>
														</div>
														<a href="" data-ng-hide="showallcat || ctrl.General.getFiltriMode().categoria != ''" data-ng-click="showallcat=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
													</div>
													<div data-ng-show="showallcat || ctrl.General.getFiltriMode().categoria != ''">
														<div class="search-filter-ckgroup">
															<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" />"
																data-ng-if="ctrl.General.getFiltriMode().categoria == ''"
																data-ng-checked="ctrl.Ricerca.novita_isChecked()"
																md-indeterminate="ctrl.Ricerca.novita_isIndeterminate()"
																data-ng-click="ctrl.Ricerca.novita_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" /></label>
															</md-checkbox>
															<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_novita_chk()"
																data-ng-if="ctrl.General.getFiltriMode().categoria == 'Novità' || ctrl.General.getFiltriMode().categoria == ''">
																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_novita_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_novita_selected())" aria-label="{{item.name}}">
																<label data-ng-bind-html="item.name"></label>
																</md-checkbox>
															</div>
														</div>
														<div class="search-filter-ckgroup">
															<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" />"
																data-ng-if="ctrl.General.getFiltriMode().categoria == ''"
																data-ng-checked="ctrl.Ricerca.docs_isChecked()"
																md-indeterminate="ctrl.Ricerca.docs_isIndeterminate()"
																data-ng-click="ctrl.Ricerca.docs_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" /></label>
															</md-checkbox>
															<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_docs_chk()"
																data-ng-if="ctrl.General.getFiltriMode().categoria == 'Documenti' || ctrl.General.getFiltriMode().categoria == ''">
																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_docs_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_docs_selected())" aria-label="{{item.name}}">
																<label data-ng-bind-html="item.name"></label>
																</md-checkbox>
															</div>
															<a href="" data-ng-hide="ctrl.General.getFiltriMode().categoria != ''" data-ng-click="showallcat=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_argomentoTab() == 'active'? 'tab-pane active' : 'tab-pane'" id="argomentoTab">
											<div class="row">
												<div class="offset-md-1 col-md-10 col-sm-12">
													<div class="search-filter-ckgroup">
														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()">
															<div data-ng-if = "$index <= 11">
																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
																<label data-ng-bind-html="item.name"></label></md-checkbox>
															</div>
															<div data-ng-if = "$index >= 12">									
																<div data-ng-show="showallarg">
																	<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
																	<label data-ng-bind-html="item.name"></label></md-checkbox>
																</div>
															</div>
														</div>
														<a href="" data-ng-hide="showallarg" data-ng-click="showallarg=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
														<a href="" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
													</div>
												</div>
											</div>
										</div>
										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_opzioniTab() == 'active'? 'tab-pane active' : 'tab-pane'" id="opzioniTab">
											<div class="row">
												<div class="offset-lg-1 col-lg-4 col-md-6 col-sm-12">
													<div class="form-check form-check-group">
														<div class="toggles">
															<label for="active_chk">
																<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT" />
																<input type="checkbox" id="active_chk" data-ng-model="ctrl.Ricerca.activeChk">
																<span class="lever"></span>
															</label>
														</div>
													</div>
													<p class="small"><@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT_INFO" /></p>
												</div>
												<div class="col-md-3 col-sm-12 search-filter-dategroup">
													<div class="form-group">
														<label for="datepicker_start"><@wp.i18n key="CITTAMETRO_PORTAL_START_DATE" /></label>
														<input type="text" class="form-control" id="datepicker_start" data-ng-model="ctrl.Ricerca.dataInizio" placeholder="gg/mm/aaaa" />
														<button aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_OPEN_CALENDAR" />" type="button" class="ico-sufix" onclick="$('#datepicker_start').focus();" onkeypress="$('#datepicker_start').focus();"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-event"></use></svg></button>
													</div>
												</div>
												<div class="col-md-3 col-sm-12 search-filter-dategroup">
													<div class="form-group">
														<label for="datepicker_end"><@wp.i18n key="CITTAMETRO_PORTAL_END_DATE" /></label>
														<input type="text" class="form-control" id="datepicker_end" data-ng-model="ctrl.Ricerca.dataFine" placeholder="gg/mm/aaaa" />
														<button aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_OPEN_CALENDAR" />" type="button" class="ico-sufix" onclick="$('#datepicker_end').focus();" onkeypress="$('#datepicker_end').focus();"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-event"></use></svg></button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
