<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>

<div class="col-lg-3 col-md-3 col-sm-6 footer-ammtrasp footer-link">
 	<h4>
	 <!--
        <a href="<@wp.url page="amministrazione_trasparente" />" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" />: 
            <@wp.pageInfo pageCode="amministrazione_trasparente" info="title" />">
            <@wp.pageInfo pageCode="amministrazione_trasparente" info="title" />
        </a>
	-->
	<@wp.i18n key="CITTAMETRO_FOOTER_TRASPARENZA" />
    </h4>
	<ul class="footer-list clearfix footer-link">
	    <li><p><@wp.i18n key="CITTAMETRO_FOOTER_AMMTRASP_TEXT" /></p></li>
		<li><a href="<@wp.url page="amministrazione_trasparente" />" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" />: 
            <@wp.pageInfo pageCode="amministrazione_trasparente" info="title" />">
			<@wp.pageInfo pageCode="amministrazione_trasparente" info="title" />
        </a></li>
	</ul>
 	
	
</div>



<div class="col-lg-3 col-md-3 col-sm-6 footer-contatti">
 	<h4>
        <@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_TITLE" />
    </h4>
 	<p>
 	 	<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" />:<br />
 	 	<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_ADDRESS" /><br />
 	 	<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_CODFISC" /> / <abbr title="<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_PIVA" />"><@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_PIVA_ABBR" />:</abbr> <@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_PIVA_VAL" /><br />
 	 	 <abbr title="<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_IBAN_ACRO" />">IBAN</abbr>: <@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_IBAN_VAL" />
 	</p>
 	<ul class="footer-list clearfix footer-link">
		<li><a href="<@wp.url page="posta_elettronica_certificata" />" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" />: <@wp.pageInfo pageCode="posta_elettronica_certificata" info="title" />"><@wp.pageInfo pageCode="posta_elettronica_certificata" info="title" /></a></li>
	</ul>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 footer-contatti">
 	<h4><span class="sr-only"><@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_NUMBERS" /></span>&nbsp;</h4>
 	<p class="footer-link"> 
	    <@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_CENTRAL" /><br />
	  	<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_GREENNUM" /><br />
 	 	<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_WHTPP" /><br />
 	</p>
<ul class="footer-list clearfix footer-link">
		<li><a href="<@wp.url page="organizzazione">
<#--
<@wp.parameter name="contentId" value="ORG9607" />
-->
</@wp.url>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_URP" />"><@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_URP" /></a></li>
		<li><a href="<@wp.url page="scrivi_comune" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="scrivi_comune" info="title" />"><@wp.pageInfo pageCode="scrivi_comune" info="title" /></a></li>
		<li><a href="<@wp.url page="amm_05_01" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="amm_05_01" info="title" />"><@wp.pageInfo pageCode="amm_05_01" info="title" /></a></li>
	</ul>
</div>




<div class="col-lg-3 col-md-3 col-sm-6 footer-seguici">
 	<h4><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL" /></h4>
 	<ul class="list-inline text-left social">

	 <#--
	 	<li class="list-inline-item">
					<a 1target="_blank" 
						href="https://twitter.com/CittaCa?s=08" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Twitter - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-twitter"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" /></span>
					</a>
				</li>
	-->

				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.facebook.com/cittametropolitanadicagliari/" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Facebook - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-facebook"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" /></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.youtube.com/channel/UCuhKPaORUxFGD6rY5DLhIzg" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- YouTube - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-youtube"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.instagram.com/cittametroca/?utm_source=ig_profile_share&igshid=vz1lk7zotn77" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Instagram - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-instagram"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span>
					</a>
				</li>
 	</ul>
<h4 class="mt48"><@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_PARTNERSHIP" /></h4>
	<div class="loghi_partnership">
		<a target="blank" aria-label="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_PONMETRO_ARIA" />" href="https://www.consregsardegna.it/corecom/" title="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_CORECOM" />"><img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/CORECOM_logo.svg" alt="<@wp.i18n key="CITTAMETRO_FOOTER_LOGO_CORECOM" />" /></a>
	</div>
</div>