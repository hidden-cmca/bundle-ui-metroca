<#assign wp=JspTaglibs["/aps-core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/metroca-core.tld"]>

<div class="logo-burger  
    <#-- 
        <#if sessionScope.currentUser != "guest">-user</#if> 
    -->">
    <div class="logoimg-burger">
        <a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"> 
            <img src="<@wp.resourceURL/>/cittametro-homepage-bundle/static/img/logo_cittametro_cagliari_verde.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_IMGALT" />" />
        </a>
    </div>
    <div class="logotxt-burger">
        <a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" /></a>
    </div>
</div>

<h2 class="sr-only"><@wp.i18n key="CITTAMETRO_HEADER_MENU_TITLE" /></h2>

<@wp.currentPage param="code" var="paginaCorrente" />
<@wp.freemarkerTemplateParameter var="paginaCorrente" valueName="paginaCorrente" />
<@uc.metrocaPageInfoTag pageCode="${paginaCorrente}" info="pathArray" var="pathCorrente"/> 

<#assign "livelloPrecedente"=-1 />
<#assign "livello"=0 />

<ul class="nav navmenu">

<@wp.nav var="pagina" spec="code(homepage).subtree(1)">
        <#if paginaPrecedente??>
            <#assign livelloPrecedente=paginaPrecedente.level />
            <#assign livello=pagina.level />
        </#if>
        <#assign "aURL"=pagina.url />

<#if 0 == livelloPrecedente>
        </li>
    </#if>

<#if livello == livelloPrecedente>
    </li>
    <li>
<#elseif livello gt livelloPrecedente>
    <#if livello gt 1>
        <ul>
    </#if>
    <li>
<#elseif livello lt livelloPrecedente>
    </li>
    <#list livello..livelloPrecedente-1 as x>
        </ul></li>
    </#list>
 <li>
</#if>

    <#assign "aperto" = false />

    <#list pathCorrente as pgitem>
 
        <#if (pgitem == pagina.code) && (pagina.code != "homepage")>
            <#assign "aperto" = true />
        </#if>

    </#list>

    <a href="${aURL}" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> :  ${pagina.title}"  <#if aperto  || (pagina.code == paginaCorrente)> class="current"</#if> >
        ${pagina.title}
    </a>

    <#assign "paginaPrecedente"= pagina />

</@wp.nav>

<#if 1 == livelloPrecedente>
</li>
</#if>
<#list livello..livelloPrecedente as x>
</li></ul>
</#list>
</ul>