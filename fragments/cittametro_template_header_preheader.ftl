<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>

<#--
<#assign jpavatar=JspTaglibs["/jpavatar-apsadmin-core"]>
-->

<!-- Fascia Appartenenza -->
<section class="preheader bg-bluscuro">
	<div class="container">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 entesup">

				<a aria-label="<@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_LINKTITLE" />" title="<@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_TEXT_TITLE" />" href="http://www.regione.sardegna.it" target="_blank">
					<@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_TEXT" escapeXml=false />
				</a>
				<div class="float-right">
					<!-- siti verticali -->
					<div class="sitiverticali float-left text-right">
						<a aria-label="<@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_OLDSITE_ARIA" />" title="<@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_OLDSITE" />" href="<@wp.url page="old_site" />"><@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_OLDSITE" /></a>
					</div>
					<!-- siti verticali -->


					<!-- accedi -->
					<div class="accedi float-left text-right">
                        <@wp.fragment code="login_prova" escapeXml=false />  
	
				</div>

			</div>
		</div>
	</div>       
</div>       
      
</section>
<!-- Fascia Appartenenza -->
