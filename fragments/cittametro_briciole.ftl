<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_YOUAREIN" />">
					<ol class="breadcrumb">
						<@wp.currentPage param="code" var="currentViewCode" />
						<@wp.freemarkerTemplateParameter var="currentViewCode" valueName="currentViewCode" />
						<#assign first=true />
						<#assign lastPageTitle="" />
						
						<@uc.cagliariMainFrameContentTag var="content"/>
						<@uc.cagliariPageInfoTag pageCode="${currentViewCode}" info="isPublishOnFly" var="isPublishOnFly"/>
						<@wp.nav spec="current.path" var="currentTarget">
							<#assign currentCode = currentTarget.code />
							<#if !currentTarget.voidPage>
								<#if currentCode == currentViewCode>
									<#if (isPublishOnFly == "true")>
										<#if (content.getAttributeByRole('jacms:title'))??>
											<#assign title=content.getAttributeByRole('jacms:title').text>
											<li class="breadcrumb-item active" aria-current="page"><a>${title}</a></li>
										</#if>
									<#else>									
										<li class="breadcrumb-item active" aria-current="page"><a>${currentTarget.title}</a></li>
									</#if>
								<#else>
									<li class="breadcrumb-item"><a href="${currentTarget.url}" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" /> ${currentTarget.title}"><strong>${currentTarget.title}</strong></a><span class="separator">/</span></li>
									<#if (!first)>
										
									</#if>
								</#if>
							<#else>
								<li class="breadcrumb-item active" aria-current="page"><a>${currentTarget.title}</a></li>
							</#if>
							<#assign lastPageTitle = currentTarget.title />
						</@wp.nav>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>