<#assign wp=JspTaglibs["/aps-core"]>
<#--
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign jpavatar=JspTaglibs["/jpavatar-apsadmin-core"]>
-->

<@wp.ifauthorized groupName="cittadini" var="isCittadino"/>
<@wp.ifauthorized groupName="administrators" var="isAdmin"/>

<#--
<#if Session.currentUser?? && Session.currentUser != "guest" && isCittadino && !isAdmin>
	<@wp.currentPage param="code" var="paginaCorrente" />
	<@wp.currentUserProfileAttribute attributeRoleName="jpspid:codicefiscale" var="utenteCodiceFiscale" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:firstname" var="utenteName" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:surname" var="utenteSurname" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:sex" var="utenteSex" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:email" var="utenteEmail" />
	<#assign utenteFullname= ''/>
	<#assign utenteShortname= ''/>
	<#if utenteName?? && utenteSurname??>
		<#assign utenteFullname>${utenteName} ${utenteSurname}</#assign>
		<#assign utenteShortname>${utenteName?substring(0,1)}${utenteSurname?substring(0,1)}</#assign>
	<#elseif utenteCodiceFiscale??>
		<#assign utenteFullname>${utenteCodiceFiscale}</#assign>
		<#assign utenteShortname>${utenteCodiceFiscale?substring(3,4)}${utenteCodiceFiscale?substring(0,1)}</#assign>
	</#if>
    -->
	<#assign color= (.now?long % 9) + 1/>

	<!-- Menu AP -->
	<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="menuap">
		<div class="overlay"></div>
		<div class="cbp-menu-wrapper clearfix">
			<div class="user-burger bg-blu">
				<div class="usertitle-burger">
					<div class="avatar-wrapper">
						<div class="avatar size-lg bg${color}">
                        <#--
							<#if (currentAvatar?has_content)>
								<img src="${currentAvatar}" alt="${utenteShortname}" />
							<#else>
								${utenteShortname}
							</#if>
                        -->
							<!--<span class="avatar-notifiche">3 <em class="sr-only">nuove notifiche</em></span>-->
						</div>
					</div>
					<span>Bentornat
                    <#--
                    <#if utenteSex?? && utenteSex == 'F'>a<#else>o</#if><#if utenteName??><br /><strong>${utenteName}</strong></#if>!<span>
					<a href="<@wp.url page="ap_profilo" />" title="<@wp.i18nITTAMETRO" />: <@wp.pageInfo pageCode="ap_profilo" info="title" />">
						<svg class="icon"><use xlink:href="<@wp.imgURL />ponmetroca.svg#ca-settings"></use></svg>
					</a>
                    -->
                    </span>
				</div>
                <#--
				<#if utenteEmail??><div class="usermail-burger"><a>${utenteEmail}</a></div></#if>
                -->
			</div>
			<div class="logo-burger-user">
				<div class="logoimg-burger">
					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"> 
						<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/logo_cittametro_cagliari.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_IMGALT" />"/>
					</a>
				</div>
				<div class="logotxt-burger">
					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" /></a>
				</div>
			</div>

			<h2 class="sr-only"><@wp.i18n key="CITTAMETRO_HEADER_MENU_TITLE" /> - <@wp.i18n key="CITTAMETRO_RESERVED_AREA" /></h2>
			
			<ul class="nav navmenu">
				<li>
					<a href="<@wp.url page="ap" />" class="list-item" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" escapeXml=false /> <@wp.pageInfo pageCode="ap" info="title" />">
							<@wp.pageInfo pageCode="ap" info="title" />
					</a>
				</li>
                <#--
				<@wp.nav var="page" spec="code(ap).subtree(1)">
					<li>
						<a href="<@wp.url page="${page.code}" />" class="list-item <#if (page.code == paginaCorrente)>current</#if>" title="<@wp.i18nITTAMETRO" escapeXml=false /> ${page.title}">
							${page.title}
						</a>
					</li>
				</@wp.nav>
				<li>
					<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action" class="list-item" title="<@wp.i18nITTAMETRO" escapeXml=false /> <@wp.i18n key="CITTAMETRO_RESERVEDAREA_LOGOUT" escapeXml=false />">
						<@wp.i18n key="CITTAMETRO_RESERVEDAREA_LOGOUT" escapeXml=false />
					</a>
				</li>
                -->
			</ul>
		</div>
	</nav>
	<!-- End Menu AP -->
    <#--
</#if>
-->