<#--
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
-->
<#assign wp=JspTaglibs["/aps-core"]>
 
<@wp.pageWithWidget var="searchResultPageVar" widgetTypeCode="jpfacetnav_results" listResult=false />

<#assign currentUser= '' />
<#assign accessToken = '' />
<#assign refreshToken = '' />
<#if (Session.currentUser??)>
	<#assign currentUser = Session.currentUser />
	<#if (Session.currentUser.accessToken??)>
		<#assign accessToken = Session.currentUser.accessToken />
	</#if>
	<#if (Session.currentUser.refreshToken??)>
		<#assign refreshToken = Session.currentUser.refreshToken />
	</#if>
</#if>


<#-- Intestazione  -->
<div class="container header" data-ng-controller="ctrlRicerca as ctrl" data-ng-init="ctrl.General.setCurrentUser('${currentUser}'); ctrl.General.setAccessToken('${accessToken}'); ctrl.General.setRefreshToken('${refreshToken}')">

<div class="container header">
	<div class="row clearfix">
		<div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 comune">
			<div class="logoprint">
				<h1>
					<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/STEMMA_CMDCA_20170329_TRATTO.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_IMGALT" />"/>
					<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" />
				</h1>
			</div>
			<div class="logoimg">
				<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"> 
					<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/STEMMA_CMDCA_20170329_TRATTO.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_IMGALT" />"/>
				</a>
			</div>
			<div class="logotxt">
				<h1>
					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" /></a>
				</h1>
			</div>
			<!-- pulsante ricerca mobile -->
			<div class="p_cercaMobile clearfix">
				<button aria-label="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SEARCH" />" class="btn btn-default btn-cerca pull-right" data-target="#searchModal" data-toggle="modal" type="button">
					<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
				</button>
			</div>
			<!-- pulsante ricerca mobile -->
		</div>
		
		<div class="col-xl-4 col-lg-4 d-none d-lg-block d-md-none pull-right text-right">
			<!-- social-->
			<ul class="list-inline text-right social">
				<li class="small list-inline-item"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL" /></li>
				
				<#--
				<li class="list-inline-item">
					<a 1target="_blank" 
						href="https://twitter.com/CittaCa?s=08" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Twitter - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-twitter"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" /></span>
					</a>
				</li>
				-->
				
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.facebook.com/cittametropolitanadicagliari/" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Facebook - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-facebook"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" /></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.youtube.com/channel/UCuhKPaORUxFGD6rY5DLhIzg" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- YouTube - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-youtube"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.instagram.com/cittametroca/?utm_source=ig_profile_share&igshid=vz1lk7zotn77" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Instagram - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-instagram"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span>
					</a>
				</li>
			</ul>
			<!-- social-->
		</div>

		<div class="col-xl-2 col-lg-2 col-md-4 d-none d-md-block d-md-none text-right">
			<!-- ricerca -->
			<div class="cerca float-right">
				<span><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SEARCH" /></span>
				<button aria-label="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SEARCH" />" class="btn btn-default btn-cerca pull-right" type="button" data-toggle="modal" data-target="#searchModal">
					<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
				</button>
			</div>
			<!-- ricerca -->
		</div>
	</div>
</div>
<!-- Intestazione -->
