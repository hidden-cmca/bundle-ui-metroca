<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>    
	
<!--[if lt IE 8]>
	<p class="browserupgrade"><@wp.i18n key="CAGLIARI_PORTAL_BROWSERUPDATE" escapeXml=false /></p>
<![endif]-->
<div class="skiplink sr-only">
	<ul>
		<li><a accesskey="2" href="<@wp.url />#main_container"><@wp.i18n key="CITTAMETRO_PORTAL_SKIPLINK_GOCONTENT" /></a></li>
		<li><a accesskey="3" href="<@wp.url />#menup"><@wp.i18n key="CITTAMETRO_PORTAL_SKIPLINK_GONAVIGATION" /></a></li>
		<li><a accesskey="4" href="<@wp.url />#footer"><@wp.i18n key="CITTAMETRO_PORTAL_SKIPLINK_GOFOOTER" /></a></li>
	</ul>
</div>