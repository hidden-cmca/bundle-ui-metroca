<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>  

<section class="postFooter clearfix">
	<div class="link_piede_footer">
		<h3 class="sr-only"><@wp.i18n key="CITTAMETRO_FOOTER_LINKUTILI_TITLE" /></h3>
		<a href="<@wp.url page="privacy" />" title="<@wp.pageInfo pageCode="privacy" info="title" />" lang="en"><@wp.pageInfo pageCode="privacy" info="title" /></a> | 
		<a href="<@wp.url page="note_legali" />" title="<@wp.pageInfo pageCode="note_legali" info="title" />"><@wp.pageInfo pageCode="note_legali" info="title" /></a> | 
		<a aria-label="<@wp.i18n key="CITTAMETRO_DICH_ACCESSIBILITA_ARIA" />" title="<@wp.i18n key="CITTAMETRO_DICH_ACCESSIBILITA_TEXT_TITLE" />"  href="https://form.agid.gov.it/view/32e1145b-80b3-4895-9a2c-5bf70b406ceb" target="_blank"> <@wp.i18n key="CITTAMETRO_DICH_ACCESSIBILITA_TEXT" escapeXml=false /></a> | 
		<a href="<@wp.url page="redazione" />" title="<@wp.pageInfo pageCode="redazione" info="title" />"><@wp.pageInfo pageCode="redazione" info="title" /></a>
	</div>
	<div class="loghi_progetto">
		<div class="lista_loghi_progetto">
			<span class="loghi_progetto_img logo_progetto_1"><a target="blank" aria-label="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_PONMETRO_ARIA" />" href="http://www.ponmetro.it/home/ecosistema/viaggio-nei-cantieri-pon-metro/pon-metro-cagliari/" title="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_PONMETRO" />"><img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/footer_ponmetro.svg" alt="<@wp.i18n key="CITTAMETRO_FOOTER_LOGO_PONMETRO" />" /></a></span>
			<span class="loghi_progetto_img logo_progetto_2"><a target="blank" aria-label="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_REPUBBLICA_ARIA" />" href="http://www.agenziacoesione.gov.it" title="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_REPUBBLICA" />"><img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/footer_repubblica.svg" alt="<@wp.i18n key="CITTAMETRO_FOOTER_LOGO_REPUBBLICA" />" /></a></span>
			<span class="loghi_progetto_img logo_progetto_3"><a target="blank" aria-label="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_UE_ARIA" />" href="https://europa.eu/european-union/index_it" title="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_UE" />"><img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/footer_ue.svg" alt="<@wp.i18n key="CITTAMETRO_FOOTER_LOGO_UE" />" /></a></span>
		</div> 
		<div class="testo_progetto">
			<p><@wp.i18n key="CITTAMETRO_FOOTER_TXT_PROGETTO" /></p>
		</div>
	</div> 
</section>