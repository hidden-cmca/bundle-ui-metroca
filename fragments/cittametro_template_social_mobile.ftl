<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>

<li class="small"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL" /></li>

<#--
<li class="">
    <a target="_blank" 
        aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> - Twitter - <@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
        href="https://twitter.com/Comune_Cagliari" 
        title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" />">
            <svg class="icon"><use xlink:href="<@wp.resourceURL />/cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-twitter"></use>
            </svg><span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" /></span></a></li>
-->

<li>
    <a target="_blank" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> - Facebook - <@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" href="https://www.facebook.com/comunecagliarinews.it" title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />/cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-facebook"></use></svg><span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" /></span></a></li>
<li>
    <a target="_blank" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> - YouTube - <@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" href="https://www.youtube.com/user/ComuneCagliariNews" title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />/cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-youtube"></use></svg><span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span></a></li>
<li>
    <a target="_blank" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> - Instagram - <@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" href="https://www.instagram.com/cittametroca/?utm_source=ig_profile_share&igshid=vz1lk7zotn77" title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_IG" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />/cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-instagram"></use></svg><span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TG" /></span></a></li>
