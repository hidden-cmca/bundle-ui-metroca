<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>  
 
<div id="topcontrol" class="topcontrol bg-bluscuro" title="<@wp.i18n key="CITTAMETRO_PORTAL_BACKTOTOP" />">
 	<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
</div>