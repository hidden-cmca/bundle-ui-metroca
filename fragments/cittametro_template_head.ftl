<#assign wp=JspTaglibs["/aps-core"]>

<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>
<#assign jpseo=JspTaglibs["/jpseo-aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign jacms=JspTaglibs["/jacms-aps-core"]>

 	<html lang="<@wp.info key="currentLang" />">
 	<head>
		<base href="<@wp.info key="systemParam" paramName="applicationBaseURL" />">
 		<!--[if IE]><script nonce="<@wp.cspNonce />" type="text/javascript" >
 			(function() {
 				var baseTag = document.getElementsByTagName('base')[0];
 				baseTag.href = baseTag.href;
 			})();
 		</script><![endif]-->
		<@wp.i18n key="CITTAMETRO_PORTAL_TITLE" var="siteName"/>
  		<@wp.currentPage param="code" var="pageCode" />    
  		<@wp.currentPage param="title" var="pageTitle" />

		<@ca.cagliariPageInfoTag pageCode="${pageCode}" info="isPublishOnFly" var="isPublishOnFly"/>
		<#if (isPublishOnFly == "true")>
			<@ca.cagliariMainFrameContentTag var="content"/>
			<#if content??>
				<#if (content.getAttributeByRole('jpsocial:title'))??>
					<#assign title=content.getAttributeByRole('jpsocial:title').text>
					<#if title??>
						<#assign pageTitle = title>
					</#if>
				</#if>
				<#if (content.getAttributeByRole('jpsocial:description'))??>
					<#assign description=content.getAttributeByRole('jpsocial:description').text>    
				</#if>
				<#if (content.getAttributeByRole('jpsocial:image'))??>
					<#assign image=content.getAttributeByRole('jpsocial:image').getImagePath("4")>
				</#if>
			</#if>
		</#if>

		<#if (pageCode == "homepage")>
			<title>${siteName}</title>
		<#else>
			<#if (pageCode == "arg_01_dett")>
				<@cw.currentArgument var="currentArg" />
				<#if (currentArg??)>
					<@cw.contentList listName="contentList" contentType="ARG" category="${currentArg}"/>
					<#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>
						<#list contentList as contentId>
							<title>${siteName} | <@jacms.content contentId="${contentId}" modelId="400012" /></title>
							<#assign description><@jacms.content contentId="${contentId}" modelId="400013" /></#assign>
						</#list>
					</#if>
					<#assign contentList="">
				</#if>
			<#else>
				<title>${siteName} | ${pageTitle}</title>
			</#if>
		</#if>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

 		<@jpseo.currentPage param="description" var="metaDescr" />
 		<#if metaDescr?? && metaDescr?has_content>
			<meta name="description" content="${metaDescr}" />
		<#else>
			<#if description??>
				<meta name="description" content="${description}" />
			<#else>
				<@ca.cagliariMainFrameContentTag var="content"/>  <#--   DA CAMBIARE -->
				<#if content??>
					<#assign description=content.getAttributeByRole('jacms:description').text>
					<meta name="description" content="${description}" />
				</#if>
			</#if>
		</#if>
		<@jpseo.currentPage param="keywords" var="metaKeyword" />
		<#if metaKeyword??>
			<meta name="keywords" content="${metaKeyword}" />
		</#if>

 		<link rel="apple-touch-icon" sizes="57x57" href="<@wp.imgURL />apple-icon-57x57.png" />
 		<link rel="apple-touch-icon" sizes="60x60" href="<@wp.imgURL />apple-icon-60x60.png" />
 		<link rel="apple-touch-icon" sizes="72x72" href="<@wp.imgURL />apple-icon-72x72.png" />
 		<link rel="apple-touch-icon" sizes="76x76" href="<@wp.imgURL />apple-icon-76x76.png" />
 		<link rel="apple-touch-icon" sizes="114x114" href="<@wp.imgURL />apple-icon-114x114.png" />
 		<link rel="apple-touch-icon" sizes="120x120" href="<@wp.imgURL />apple-icon-120x120.png" />
 		<link rel="apple-touch-icon" sizes="144x144" href="<@wp.imgURL />apple-icon-144x144.png" />
 		<link rel="apple-touch-icon" sizes="152x152" href="<@wp.imgURL />apple-icon-152x152.png" />
 		<link rel="apple-touch-icon" sizes="180x180" href="<@wp.imgURL />apple-icon-180x180.png" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-32x32.png" sizes="32x32" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />android-chrome-192x192.png" sizes="192x192" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-96x96.png" sizes="96x96" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-16x16.png" sizes="16x16" />





 <!--   ##########BUONO############   -->
<meta charset="utf-8" />
      	<title><@wp.currentPage param="title" /></title>
      	<meta name="viewport" content="width=device-width,  user-scalable=no" />
 		<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery-3.3.1.min.js" ></script>        
		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/angular-material.cagliari.css"  />         
		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/angular-material.css"  />  
		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/font/fonts.min.css" />
        <link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/bootstrap-italia_1.2.0.min.css" />

 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/cittametro.css" rel="stylesheet" type="text/css" />
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/areapersonale.css" rel="stylesheet" type="text/css" />
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/cagliari-print.css" rel="stylesheet" type="text/css" media="print" />
 		<@wp.outputHeadInfo type="CSS_CA_INT">
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_EXT">
 		<link href="<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_PLGN">
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 
 		<@wp.outputHeadInfo type="JS_CA_TOP_EXT">nonce="<@wp.cspNonce />" src="<@wp.printHeadInfo />" ></script></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="JS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="JS_CA_TOP"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/<@wp.printHeadInfo />" ></script></@wp.outputHeadInfo>


  		<@wp.i18n key="CAGLIARI_PORTAL_TITLE" var="siteName"/>
  		<@wp.currentPage param="code" var="pageCode" />    
  		<@wp.currentPage param="title" var="pageTitle" />
		<#if pageCode?? && pageCode != "">
		<#else>
			<#assign pageCode='homepage'>
		</#if>
  		<#if pageCode == 'homepage'>
  		 	<#assign pageTitle=siteName>
  		</#if>
     
  		<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" var="description"/>
        <#--
  		<@wp.info key="systemParam" paramName="applicationBaseURL" var="baseURL"/>
		<#assign baseURL = baseURL?substring(0,baseURL?last_index_of("/portale"))>
		 FACEBOOK 
		<meta property="og:site_name" content="${siteName}" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="${pageTitle}" />
		<#if description??>
			<meta property="og:description" content="${description}"/>
		<#else>
			<meta property="og:description" content="-"/>
		</#if>
		<#if image?? && image != "">
			<meta property="og:image" content="${baseURL}${image}" />
		<#else>
			<meta property="og:image" content="${baseURL}<@wp.imgURL/>logo_cagliari_print.png" />
		</#if>
		<meta property="fb:app_id" content="409032892503811" />

		 TWITTER 
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:site" content="@Comune_Cagliari" />
		<meta name="twitter:title" content="${pageTitle}" />
		<#if description??>
			<meta name="twitter:description" content="${description}"/>
		<#else>
			<meta name="twitter:description" content="-"/>
		</#if>    
		<#if image?? && image != "">
			<meta name="twitter:image" content="${baseURL}${image}" />
		<#else>
			<meta name="twitter:image" content="${baseURL}<@wp.resourceURL />cittametro-homepage-bundle/static/img/logo_cittametro_cagliari.png" />
		</#if>
		<@wp.fragment code="cagliari_template_googletagmanager" escapeXml=false />
        
		-->
		
 	</head>
	 <@wp.fragment code="keycloak_auth" escapeXml=false />
