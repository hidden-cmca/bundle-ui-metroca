<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign comCa=JspTaglibs["/WEB-INF/tld/reservedPonMetro.tld"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_BTM" info="interne.css" />
			    
<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cittametro_template_googletagmanagerbody" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
					 <@wp.fragment code="cittametro_template_header_navigation_menu" escapeXml=false />
                         <ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
			    <#assign leafletjs_css>
                    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
                </#assign>
                <#assign leafletjs_js>
                    <script nonce="<@wp.cspNonce />" src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
                </#assign>
                <@wp.headInfo type="CSS_CA_EXT_GLB" info="${leafletjs_css}" />
                <@wp.headInfo type="JS_CA_EXT_GLB" info="${leafletjs_js}" />
                <@wp.headInfo type="JS_CA_TOP" info="leafletjs_config.min.js" />
			    <@wp.headInfo type="CSS_CA_BTM" info="sezioni.css" />
                <@wp.headInfo type="CSS_CA_BTM" info="interne.css" />

                <@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
                <@wp.headInfo type="CSS_CA_BTM" info="owl.carousel.css" />
                <@wp.headInfo type="CSS_CA_BTM" info="owl.theme.default.css" />
                <@wp.headInfo type="JS_CA_BTM" info="filtri-controller.js" />
                <@wp.headInfo type="JS_CA_BTM" info="filtri-service.js" />

                <#assign js_slide="$(document).ready(function() {
                	var owl = $('#owl-correlati');
                	owl.owlCarousel({
                		nav:false,
                		startPosition: 0,
                		autoPlay:false,
                		responsiveClass:true,
                		responsive:{
                				0:{
                					items:1,
                				},
                				576: {
                					items:2,
                				},
                				768: {
                					items:2,
                				},
                				991: {
                					items:4,
                				},
                			}
                	});

                	var owl2 = $('#owl-galleria');
                	owl2.owlCarousel({
                		nav:false,
                		startPosition: 0,
                		autoPlay:false,
                		responsiveClass:true,
                		responsive:{
                				0:{
                					items:1,
                				},
                				576: {
                					items:2,
                				},
                				768: {
                					items:2,
                				},
                				991: {
                					items:3,
                				},
                			}
                	});

                });" />
                <@wp.headInfo type="JS_CA_BTM_INC" info="${js_slide}" />
			
			    <@wp.fragment code="cittametro_template_briciole" escapeXml=false />
				<section id="intro">
                    <div class="container">
                    	<div class="row">
                    		<div class="offset-lg-1 col-lg-6 col-md-7">
                    			<div class="titolo-sezione">
                    				<@wp.show frame=2 />
                    			</div>
                    		</div>
                    		<div class="offset-lg-1 col-lg-3 offset-lg-1 offset-md-1 col-md-4">
                    			<aside id="menu-sezione">
                    				<@wp.show frame=3 />
                    			</aside>
                    		</div>
                    	</div>
                    </div>
				</section>
				<@comCa.serviziOnlineAuthorityChecker authLevel="cittadini" var="testlogin">
					<section>
						<div class="container">
							<@wp.show frame=4 />
						</div>
					</section>
					<section>
						<div class="container">
							<@wp.show frame=5 />
						</div>
					</section>
					<section>
						<div class="container">
							<@wp.show frame=6 />
						</div>
					</section>
				</@comCa.serviziOnlineAuthorityChecker>
				<@wp.freemarkerTemplateParameter var="testlogin" valueName="testlogin" removeOnEndTag=true >
					<#if testlogin?? && !testlogin>
						<@wp.fragment code="cittametro_unauthorized_alert_service_cittadino" escapeXml=false />
					</#if>
				</@wp.freemarkerTemplateParameter>
                <section>
	                <div class="container">
	                    <@wp.show frame=7 />
				    </div>
                </section>
			</main>
			<footer id="footer" class="mt100">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=8 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
		<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	</body>
</html>