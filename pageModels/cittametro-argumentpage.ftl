<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.css" />

<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
	<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cittametro_template_googletagmanagerbody" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
					 <@wp.fragment code="cittametro_template_header_navigation_menu" escapeXml=false />
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 						    <@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
				<div class="bg-grigio">
					<section id="intro-argomenti">
						<@wp.show frame=2 />
					</section>
					<section id="argomenti-inevidenza" class="sezioni">
						<div class="container">
							<@wp.show frame=3 />
						</div>
					</section>
				</div>
				<section id="argomenti-amministrazione">
					<div class="container">
						<@wp.show frame=4 />
					</div>
				</section>
				<section id="argomenti-servizi">
					<div class="container">
						<@wp.show frame=5 />
					</div>
				</section>
				<section id="argomenti-novita" class="sezioni bg-grigio">
					<div class="container">
						<@wp.show frame=6 />
					</div>
				</section>
				<section id="argomenti-documenti">
					<div class="container">
						<@wp.show frame=7 />
					</div>
				</section>
				<section id="argomenti-gallerie">
					<div class="container">
						<@wp.show frame=8 />
					</div>
				</section>
				<@wp.show frame=9 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=11 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
	</body>
</html>