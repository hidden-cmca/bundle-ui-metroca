## è necessario il widget    cagliari_widget_advsearch_results   (installare tramite  /hidden-deployments/slq/script-searchCA.sh)

widget cittametro_navbar_orizzontale utilizzabile solo con immagine hidden-cmca/entando-de-app-wildfly/0.0.4 (servono i tag di metroca-core)

per installazione con ent da git

`ent bundler from-git --repository=https://gitlab.com/hidden-cmca/bundle-ui-metroca.git --namespace=entando --name=bundle-ui-metroca`

per installazione senza ent con entando bundler (npm install -g @entando/entando-bundler@6.3.2)

`entando-bundler from-git --repository=https://gitlab.com/hidden-cmca/bundle-ui-metroca.git --namespace=entando --name=bundle-ui-metroca --dry-run | sudo kubectl -n entando apply -f -`

